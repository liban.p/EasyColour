" Easy Colour:
"   Author:  A. S. Budden <abudden _at_ gmail _dot_ com>
" Copyright: Copyright (C) 2011 A. S. Budden
"            Permission is hereby granted to use and distribute this code,
"            with or without modifications, provided that this copyright
"            notice is copied with it. Like anything else that's free,
"            the EasyColour plugin is provided *as is* and comes with no
"            warranty of any kind, either expressed or implied. By using
"            this plugin, you agree that in no event will the copyright
"            holder be liable for any damages resulting from the use
"            of this software.

" ---------------------------------------------------------------------

let g:easycolour_hl_group = {
        \ '01-default' : [
            \ 'ColorColumn'  , 'Conceal'      ,
            \ 'Cursor'       , 'CursorColumn' ,
            \ 'CursorIM'     , 'CursorLine'   ,
            \ 'CursorLineNr' , 'DiffAdd'      ,
            \ 'DiffChange'   , 'DiffDelete'   ,
            \ 'DiffText'     , 'Directory'    ,
            \ 'ErrorMsg'     , 'FoldColumn'   ,
            \ 'Folded'       , 'IncSearch'    ,
            \ 'LineNr'       , 'MatchParen'   ,
            \ 'Menu'         , 'ModeMsg'      ,
            \ 'MoreMsg'      , 'NonText'      ,
            \ 'Normal'       , 'Pmenu'        ,
            \ 'PmenuSbar'    , 'PmenuSel'     ,
            \ 'PmenuThumb'   , 'Question'     ,
            \ 'Scrollbar'    , 'Search'       ,
            \ 'SignColumn'   , 'SpecialKey'   ,
            \ 'SpellBad'     , 'SpellCap'     ,
            \ 'SpellLocal'   , 'SpellRare'    ,
            \ 'StatusLine'   , 'StatusLineNC' ,
            \ 'TabLine'      , 'TabLineFill'  ,
            \ 'TabLineSel'   , 'Title'        ,
            \ 'Tooltip'      , 'VertSplit'    ,
            \ 'Visual'       , 'VisualNOS'    ,
            \ 'WarningMsg'   , 'WildMenu'     ,
        \ ],
        \
        \ '02-extended' : [
            \ 'Comment'     , 'Boolean'          , 'Character'    ,
            \ 'Conditional' , 'Constant'         , 'Debug'        ,
            \ 'Define'      , 'Delimiter'        , 'Error'        ,
            \ 'Exception'   , 'Float'            , 'Function'     ,
            \ 'Identifier'  , 'Ignore'           , 'Include'      ,
            \ 'Keyword'     , 'Label'            , 'Macro'        ,
            \ 'Number'      , 'Operator'         , 'PreCondit'    ,
            \ 'PreProc'     , 'Repeat'           , 'Special'      ,
            \ 'SpecialChar' , 'SpecialComment'   ,
            \ 'Statement'   , 'SpecialStatement' , 'StorageClass' ,
            \ 'String'      , 'Structure'        , 'Tag'          ,
            \ 'Todo'        , 'Type'             , 'Typedef'      ,
            \ 'Underlined'
        \ ],
        \
        \ '03-ctags' : [
            \ 'AutoCommand'      , 'Class'           , 'Command'   ,
            \ 'DefinedName'      , 'EnumerationName' ,
            \ 'EnumerationValue' , 'Extern'          ,
            \ 'File'             , 'GlobalVariable'  , 'Import'    ,
            \ 'LocalVariable'    , 'Map'             , 'Member'    ,
            \ 'Method'           , 'Module'          , 'Namespace' ,
            \ 'Singleton'        , 'Subroutine'      , 'Union'
        \ ],
    \}

command! -nargs=? EasyColourImport call EasyColour#Import#ColourScheme2Buf(<q-args>)


