" Easy Colour:
"   Author:  A. S. Budden <abudden _at_ gmail _dot_ com>
" Copyright: Copyright (C) 2011 A. S. Budden
"            Permission is hereby granted to use and distribute this code,
"            with or without modifications, provided that this copyright
"            notice is copied with it. Like anything else that's free,
"            the EasyColour plugin is provided *as is* and comes with no
"            warranty of any kind, either expressed or implied. By using
"            this plugin, you agree that in no event will the copyright
"            holder be liable for any damages resulting from the use
"            of this software.

" ---------------------------------------------------------------------
try
	if &cp || (exists('g:loaded_EasyColourImport') && (g:plugin_development_mode != 1))
		throw "Already loaded"
	endif
catch
	finish
endtry
let g:loaded_EasyColourImport = 1

function! s:TitleCase(word)
  return substitute(a:word,'^.','\u&','')
endfunction

let s:spec_map = {
		\ "guifg=" : 'FG=',
		\ "guibg=" : 'BG=',
		\ "guisp=" : 'SP=',
		\ "gui="   : 'Style=',
	\ }

function! s:convert_spec(list)
	let spec = join(map(filter(a:list, "v:val =~ '^gui'"), "substitute(v:val, ',', '&', '')"), ',')
	for [key, value] in items(s:spec_map)
		let spec = substitute(spec, key, value, '')
	endfor
	return spec
endfunction

function! EasyColour#Import#MissingGroupHl(hl_lines)
        let hl_group = deepcopy(g:easycolour_hl_group)
        for k in keys(hl_group)
                call filter(hl_group[k], 'match(a:hl_lines, "^\t".v:val) < 0')
        endfor
        return hl_group
endfunction

function! EasyColour#Import#Sort(hl_lines)

        let etc = '99-other'
        let hl_groups = {}
        for k in keys(g:easycolour_hl_group)
                let hl_groups[k] = []
        endfor
        let hl_groups[etc] = []


        for line in a:hl_lines
                let found = 0
                for k in keys(g:easycolour_hl_group)
                        if line =~# '^\t\%(' . join(g:easycolour_hl_group[k],'\|') . '\)\s*:'
                                call add(hl_groups[k], line)
                                let found = 1
                                continue
                        endif
                endfor
                if found == 0
                        call add(hl_groups[etc], line)
                endif
        endfor

        let hl_lines_sorted = []
        for k in sort(keys(hl_groups))
                call add(hl_lines_sorted, '# '.k.' hlgroup')
                for line in sort(hl_groups[k])
                        call add(hl_lines_sorted, line)
                endfor
                call add(hl_lines_sorted, '')
        endfor

        return hl_lines_sorted
endfunction

function! EasyColour#Import#ColourScheme(...)
        if !has("gui_running")
                throw "EasyColour: import colorscheme work from gui_running"
        endif

	if a:0 >= 1
		let colorscheme_name = a:1
		if colorscheme_name != '' && g:colors_name != colorscheme_name
			exec "colorscheme " . a:name
		endif
	endif


	let color_def = []
        call add(color_def, 'Colours:')

	let color_spec = []
	call add(color_spec, 'Basis:None')
	call add(color_spec, 'Background:'.s:TitleCase(&background))

        call add(color_spec, s:TitleCase(&background).':')
	redir => hi_output
	silent execute 'hi'
	redir END

	let entries = split(hi_output, "\n")
        let color_hl_lines = []
	for e in entries
		let p = split(e)
		let groupname = p[0]
		if p[2] == 'links' && p[3] == 'to'
			call add(color_hl_lines, "\t".groupname.':@'.p[4])
		else
			call add(color_hl_lines, "\t".groupname.':'.s:convert_spec(p[2:]))
		endif
                if groupname == 'Normal'
                        let base_colors = split(substitute(tolower(s:convert_spec(p[2:])), '=', ':', 'g'), ',')
                        call extend(color_def,map(base_colors,'"\t".v:val'))
                endif
	endfor

        call extend(color_spec, EasyColour#Import#Sort(color_hl_lines))
	call add(color_spec, '')
	call add(color_spec, '# LightAuto:True')
        call add(color_spec, '')
	call add(color_spec, '# vim: ff=unix:noet:noic:commentstring=#%s:ft=EasyColour')
	return extend(color_def, color_spec)

endfunction


function! EasyColour#Import#ColourScheme2Buf(...)
        let colorscheme_name = a:0 >= 1 ? a:1 : ''
        let color_spec = EasyColour#Import#ColourScheme(colorscheme_name)

	exec "leftabove vsplit newcolor.txt"
	% delete _
        for e in color_spec
            call append(line('$'), e)
        endfor
	1 delete _
endfunction

function! EasyColour#Import#ColourScheme2File(fname)
        let colorscheme_name = a:0 >= 1 ? a:1 : ''
        let color_spec = EasyColour#Import#ColourScheme(colorscheme_name)

        call writefile(color_spec, fnmae)
endfunction



