" Easy Colour:
"   Author:  A. S. Budden <abudden _at_ gmail _dot_ com>
" Copyright: Copyright (C) 2011 A. S. Budden
"            Permission is hereby granted to use and distribute this code,
"            with or without modifications, provided that this copyright
"            notice is copied with it. Like anything else that's free,
"            the EasyColour plugin is provided *as is* and comes with no
"            warranty of any kind, either expressed or implied. By using
"            this plugin, you agree that in no event will the copyright
"            holder be liable for any damages resulting from the use
"            of this software.

" ---------------------------------------------------------------------
try
        if &cp || (exists('g:loaded_EasyColourUtil') && (g:plugin_development_mode != 1))
                throw "Already loaded"
        endif
catch
        finish
endtry

let g:loaded_EasyColourUtil = 1

function! EasyColour#Util#SortGroupHl() range

        let saved_cursor = getpos(".")

        let saved_lazyredraw = &lazyredraw 
        set lazyredraw


        let sorted = EasyColour#Import#Sort(getline(a:firstline, a:lastline))

        exec a:firstline.','.a:lastline."delete _"

        let lno = a:firstline
        for _ in sorted
                call append(lno, _) | let lno += 1
        endfor

        call setpos('.', saved_cursor)
        redraw
        let &lazyredraw = saved_lazyredraw
endfunction

function! EasyColour#Util#MissingGroupHl() range

        return EasyColour#Import#MissingGroupHl(getline(a:firstline, a:lastline))
endfunction


" [TODO]( find missing hl groupname ) @zhaocai @start(2012-08-29 02:49)
